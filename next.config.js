const path = require('path');
const withCSS = require("@zeit/next-css")
module.exports = withCSS()

module.exports = {
  webpack: config => {
    config.resolve.modules.push(path.resolve(__dirname));
    return config;
  }
};