import 'public/assets/css/bootstrap.min.css';
import 'public/assets/scss/style.scss';

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
