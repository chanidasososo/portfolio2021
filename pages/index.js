import React from "react";
import Head from "next/head";

export default class Index extends React.Component {
  render() {
    return (
      <>
        <Head>
          <title>:: Chanida Portfolio</title>
        </Head>

        <header>
          <div className="container">
            <div className="row">
              <div className="col-md-6 head-center">
                <div>
                  <div className="port-name" id="totop">
                    <span>Chanida</span>
                    <span>Klinsucon</span>
                  </div>
                  <div className="port-position">Frontend web developer</div>
                  <div className="slogan">
                    I am enjoy to work as a website developer and capable to
                    achieve the target beyond company
                  </div>
                </div>
              </div>

              <div className="col-md-6">
                <img src="assets/images/port.jpeg" alt="" />
              </div>
            </div>
          </div>
        </header>

        <div className="headgroup">
          <div className="container">
            <div className="port-detail">
              <div className="column left">
                <div className="box-content">
                  <div className="text">
                    <h3>about me</h3>
                    <span>
                      I am enjoy to work as a website developer and <br />{" "}
                      capable to achieve the target beyond company
                    </span>
                  </div>
                  <div className="text">
                    <h3>Contact us</h3>
                    <span>Phone : 094-4380061</span>
                    <br />
                    <span>E-mail : chanidaklinsu@gmail.com</span>
                    <br />
                    <span>
                      Address : 195 Moo5 Bangkrabue, Mueng SingBuri 16000
                    </span>
                  </div>
                  <div className="text">
                    <h3>Skills</h3>
                    <h4>programming languages</h4>
                    <br />
                    <span>HTML/CSS/SCSS</span>
                    <br />
                    <span>Java Script</span>
                    <br />
                    <h4>Framework</h4>
                    <span>React</span>
                    <br />
                    <h4>Other</h4>
                    <br/>
                    <span>jQuery,</span>
                    <br />
                    <span>Responsive/Mobile Design,</span>
                    <br />
                    <span>Git,</span>
                    <br />
                    <span>ClickUp,</span>
                    <br />
                    <span>Bootstrap,</span>
                    <br />
                    <span>Visual Studio</span>
                    <br />
                    <span>figma</span>
                    <br />
                    <span>Adobe Photoshop</span>
                    <br />
                    <span>Adobe XD</span>
                    <br />
                  </div>
                </div>
              </div>

              <div className="column right">
                <div className="box-content">
                  <div className="text">
                    <h3>Education</h3>
                    <span>
                      2017 - 2020 <br /> Information technology <br />
                      Faculty of science
                    </span>
                    <br />
                    <span>Naresuan University</span>
                  </div>
                  <div className="text">
                    <h3>Experience</h3>
                    <span>
                      2020 - 2021 <br /> Front-End Web developer
                    </span>{" "}
                    <br />
                    <span>
                      Amarin Printing and Publishing <br />
                      Public Company Limited
                    </span>
                    <br />
                    <span>
                      2021 - 2024 <br /> Front-End  developer
                    </span>{" "}
                    <br />
                    <span>
                    IC Web Co., Ltd.
                    </span>
                  </div>
                  <div className="text">
                    <h3>work Experience</h3>
                    <span>amarinfair.com</span>
                    <br />
                    <span>www.kasikornbank.com/th/promotion/</span>
                  </div>
                </div>
              </div>

              {
                <div className="portfolio py-5">
                  <div className="row">
                    <div className="col-md-4">
                      <h3>Portfolio</h3>
                      <div className="bg-grey">
                      <a target="_blank" href="/port1">
                        <img src="/assets/images/port-1.png" alt="" />
                      </a>
                      </div>
                    </div>
                    <div className="col-md-4 pt-4 mt-3 bg-grey">
                      <a target="_blank" href="/port2">
                        <img src="/assets/images/port-2.png" alt="" />
                      </a>
                    </div>
                    <div className="col-md-4 pt-4 mt-3 bg-grey">
                      <a target="_blank" href="/port4">
                        <img src="/assets/images/port-4.png" alt="" />
                      </a>
                    </div>
                    <div className="col-md-4 pt-4 mt-3 bg-grey">
                      <a target="_blank" href="https://www.kasikornbank.com/th/personal/debit-card/pages/k-my.aspx">
                        <img src="/assets/images/port8.png" alt="" />
                      </a>
                    </div>
                    <div className="col-md-4 pt-4 mt-3 bg-grey">
                      <a target="_blank" href="https://www.kasikornbank.com/th/business/sme/k-business-privilege/pages/index.aspx">
                        <img src="/assets/images/port9.png" alt="" />
                      </a>
                    </div>
                    <div className="col-md-4 pt-4 mt-3 bg-grey">
                      <a target="_blank" href="https://www.kasikornbank.com/th/privacy-policy/pages/personal-data-protection.aspx">
                        <img src="/assets/images/port10.png" alt="" />
                      </a>
                    </div>
                    <div className="col-md-4 pt-4 mt-3 bg-grey">
                      <a target="_blank" href="https://uat.kasikornbank.com/th/business/sme/ksmeknowledge/article/pages/index.aspx">
                        <img src="/assets/images/port11.png" alt="" />
                      </a>
                    </div>
                    <div className="col-md-4 pt-4 mt-3 bg-grey">
                      <a target="_blank" href="https://www.kasikornbank.com/th/personal/digital-banking/pages/k-esavings-account-opening-have-no-kplus.aspx">
                        <img src="/assets/images/port12.png" alt="" />
                      </a>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
          <div className="btn">
            <a href="#"></a>
          </div>
          {/* end .container  */}
        </div>
      </>
    );
  }
}
