import React from "react";
import Head from "next/head";

export default class Port1 extends React.Component {
  render() {
    const ar = [];
    console.log(ar);
    return (
      <>
        <Head>
          <title>:: Port 1</title>
        </Head>
        <header></header>
        <div className="port1">
          <div className="slider">
            <img src="assets/images/sea-t.jpg" alt="" />
          </div>
          <div className="container">
            <div className="logo-top">
              <img src="assets/images/logotop.png" alt="" />
            </div>
            <div className="text-slider">
              <div className="text-t text">
                <h6 className="title-section">some text goes here</h6>
              </div>

              <div className="text-c text">
                <h3>
                  our mission is to make your <br /> business grow
                </h3>
              </div>

              <div className="button-menutop">
                <a href="#" className="btn-style1 top">
                  start our now
                </a>
              </div>
            </div>
          </div>

          {/* <div className="Lists">
          <a href="#" className="lists-style">HOME</a>
          <a href="#" className="lists-style">PORTFOLIO</a>
          <a href="#" className="lists-style">ABOUT</a>
          <a href="#" className="lists-style">CONTACT</a>
        </div> */}

          <div className="download">
            <div className="text-dow">
              <h6>BEST PSD TEMPLATE FOR YOUR WEBSITE.DOWNLOAD NOW</h6>
            </div>
            <div className="button-dow">
              <div className="dow">
                <a href="#">DOWNLOAD NOW</a>
              </div>
            </div>
          </div>

          <div className="container">
            <div className="history">
              <div className="text-his">
                <h3>our beginning</h3>
                <h4 className="title">CLOSER TO THE STARS</h4>
                <h6>
                  Donec rutrum congue leo eget malesuada. Curabitur non nulla
                  sit amet <br />
                  nisl tempus convallis quis ac lectus. Sed porttitor lectus
                  nibh. Proin
                  <br />
                  eget tortor risus. Nulla porttitor accumsan tincidunt. Nulla
                  porttitor
                  <br />
                  accumsan tincidunt. <br />
                  <br />
                  Curabitur aliquet quam id dui posuere blandit.
                  <br />
                  Nulla quis lorem ut libero malesuada feugiat. Donec rutrum
                  congue leo
                  <br />
                  eget malesuada. Curabitur aliquet quam id dui posuere blandit.
                </h6>
                <br />
                <a href="#" className="btn-style1">
                  see portfolio
                </a>
              </div>

              <div className="img-his">
                <img src="public/assets/images/typewriter.png" alt="" />
              </div>
            </div>
          </div>

          <div className="test">
            <div className="video">
              <div className="bg-video">
                <img src="assets/images/com-c.png" alt="" />
              </div>
              <div className="v-icon">
                <h3>see our video</h3>
                <div className="icon-row">
                  <img src="assets/images/play.png" alt="" />
                </div>
              </div>
            </div>
          </div>

          <div className="portfolio">
            <div className="text-port">
              <h3>portfolio</h3>
              <h4 className="title-section">see our best work so far</h4>
            </div>
            <div className="texe-p">
              <p>
                Donec rutrum congue leo eget malesuada. Curabitur non nulla sit
                amet nisl tempus convallis
                <br />
                quis ac lectus. Sed porttitor lectus nibh. Proin eget tortor
                risus. Nulla porttitor accumsan tincidunt.
                <br />
                Nulla porttitor accumsan tincidunt.
              </p>
            </div>
          </div>

          <div className="menu">
            <div className="menu-bt">
              <p className="btn-style">branding</p>
              <p className="btn-style1">photography</p>
              <p className="btn-style1">web design</p>
              <p className="btn-style1">app design</p>
            </div>
            <div className="menu-img">
              <div className="menu-one">
                <img src="assets/images/book.jpg" alt="" />
                <img src="assets/images/notebook.jpg" alt="" />
                <img src="assets/images/magazine.jpg" alt="" />
                <img src="assets/images/phone.jpg" alt="" />
                <img src="assets/images/glass.jpg" alt="" />
                <img src="assets/images/sofa.jpg" alt="" />
              </div>
              <br />
              <br />
              <div className="bt-seemore">
                <a href="#" className="btn-style1">
                  see more
                </a>
              </div>
            </div>{" "}
            <br />
          </div>

          <div className="ouote">
            <div className="img-ouote">
              <img src="assets/images/coffee-bc.png" alt="" />
            </div>
            <div className="text-ouote">
              <div className="img-logo">
                <img src="assets/images/logocenter.png" alt="" />
              </div>
              <h5>
                LOREM IPSUM DOLOR SIT, AD ETIAM SIT ONER <br />
                OL DEIAN BOST SIT EMIST.
              </h5>
            </div>
          </div>

          <div className="container">
            <div className="abot-us">
              <div className="text-abot text-us">
                <h2>About us</h2>
                <h4 className=" title-section">who we are</h4>
              </div>
              <div className="p-abot text-us">
                <p>
                  Donec rutrum congue leo eget malesuada. Curabitur non nulla
                  sit amet nisl tempus convallis
                  <br />
                  quis ac lectus. Sed porttitor lectus nibh. Proin eget tortor
                  risus. Nulla porttitor accumsan tincidunt.
                  <br />
                  Nulla porttitor accumsan tincidunt.
                </p>
              </div>

              <div className="abot-img">
                <div className="pic all">
                  <div className="row-pic picture">
                    <img src="assets/images/man-l.jpg" alt="" />
                  </div>
                  <div className="pic-text name">
                    <h3>John stephenson</h3>
                  </div>
                </div>
                <div className="picwoman all">
                  <div className="row-picwoman picture">
                    <img src="assets/images/woman-r.jpg" alt="" />
                  </div>
                  <div className="picwoman-text name">
                    <h3>John stephenson</h3>
                  </div>
                </div>
                <div className="man all">
                  <div className="row-man picture">
                    <img src="assets/images/man-s.jpg" alt="" />
                  </div>
                  <div className="man-text name">
                    <h3>John stephenson</h3>
                  </div>
                </div>
                <div className="manr all">
                  <div className="row-manr picture">
                    <img src="assets/images/man-r.jpg" alt="" />
                  </div>
                  <div className="manr-text name">
                    <h3>John stephenson</h3>
                  </div>
                </div>
              </div>

              <div className="big-group">
                <div className="group-one group">
                  <h3>about us</h3>
                  <p>
                    Donec rutrum congue leo eget malesuada.Curab
                    <br />
                    non nulla sit amet nisl tempus convallis quis ac
                    <br />
                    lectus. Sed porttitor nih.Proin eget tortor risus.
                    <br />
                    Nulla porttitor accumsan tinci dunt. Nulla
                    <br />
                    porttitor.
                  </p>
                </div>
                <div className="group-two group">
                  <h3>who we are</h3>
                  <p>
                    Donec rutrum congue leo eget malesuada.Curab
                    <br />
                    non nulla sit amet nisl tempus convallis quis ac
                    <br />
                    lectus. Sed porttitor nih.
                  </p>
                </div>
                <div className="group-two group">
                  <h3>why we do this</h3>
                  <p>
                    Donec rutrum congue leo eget malesuada.Curab
                    <br />
                    non nulla sit amet nisl tempus convallis quis ac
                    <br />
                    lectus. Sed porttitor nih.Proin eget tortor risus.
                    <br />
                    Nulla porttitor accumsan tinci dunt.{" "}
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className="subscribe">
            <div className="heading">
              <p>subscribe to our newsletter</p>
            </div>
            <div className="main">
              <form></form>
              <div className="box-style">
                <input type="text" placeHolder="Email address*" />
                <a href="#" className="btn">
                  subscribe
                </a>
              </div>
            </div>
          </div>

          <div className="contact">
            <div className="con-text">
              <h2>contact us</h2>
              <p className="title-section">we love talking</p>
            </div>
            <div className="con-text-container">
              <p>
                Donec rutrum congue leo eget malesuada. Curabitur non nulla sit
                amet nisl tempus convallis
                <br />
                quis ac lectus. Sed porttitor lectus nibh. Proin eget tortor
                risus. Nulla porttitor accumsan tincidunt.
                <br />
                Nulla porttitor accumsan tincidunt.
              </p>
            </div>
            <div className="icon">
              <div className="icon-com text">
                <img src="assets/images/computer.png" alt="" />
                <p>somemail@mail.com</p>
              </div>
              <div className="icon-home text">
                <img src="assets/images/home.png" alt="" />
                <p>
                  T317 Timber Oak Drive
                  <br />
                  Sundown, TX79372
                </p>
              </div>
              <div className="icon-phone text">
                <img src="assets/images/phoneh.png" alt="" />
                <p>+000-123-456-789</p>
              </div>
            </div>

            <div className="box">
              <form>
                <div className="box-text">
                  <div className="box-three">
                    <input placeholder="Subject*" pattern="Subject" />
                    <br />
                    <input placeholder="Name*" pattern="Name" />
                    <br />
                    <input placeholder="Email*" pattern="Email" />

                    <div className="box-one">
                      <textarea
                        placeholder="Message"
                        pattern="Message"
                        rows="8"
                        cols="50"
                      ></textarea>
                      <div className="submit-button">
                        <a href="#" className="btn-style btn-submit">
                          submit
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <br />

            <div className="footer">
              <img src="/assets/images/logo.png" alt="" />
              {/* <div className="socal">
                <img src="/assets/images/facebook.png" alt="" />
                <img src="/assets/images/twitter.png" alt="" />
                <img src="/assets/images/cen.png" alt="" />
                <img src="/assets/images/google.png" alt="" />
                <img src="/assets/images/red.png" alt="" />
              </div> */}
              <div className="text-foo">
                <p>2014-Wooster ALL Right Reserved</p>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
