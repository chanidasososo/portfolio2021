import React from 'react';
import Head from 'next/head';

export default class Index extends React.Component {

  render() {
    return (
      <>
        <Head>
          <title>:: port2</title>
        </Head>

        <div className="port2">
          <div className="top-menu">
            <nav className="navbar navbar-expand-md bg-white navbar-dark">
              <a className="navbar-brand" href="#"><img src="/assets/images/logoblack.png" /></a>
              <button className="navbar-toggler bg-dark" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span className="navbar-toggler-icon navbar-dark"></span>
              </button>
            </nav>
          </div>


          <div className="landing">
            <img className="landing-img" src="/assets/images/ice.jpg" />
            <div className="landing-text">
              <div className="title-section">
                <img src="/assets/images/logowhite.png" />
              </div>
              <div className="container">
                <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan<br />
                    tincidunt. Quisque velit nisi, pretium ut lacinia in, elemena porttlit nisi, pretium ut<br />
                     lacinia in elementumestibulum ac diam sit anet.</p>
              </div>
              <a href="#" className="btn">Purshase Theme</a>
            </div>
          </div>

          <div className="fully row">
            <div className="fully-text">
              <h2>FULLY LAYERED AND CUSTOMIZABLE TEMPLATE</h2>
              <h6 className="title-section">OUR SERVICES</h6>
              <div className="container-fully"><h6>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tinci<br />
                dunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lac<br />
                inia in, elementum id enim. Vestibulum ac diam sit anet.</h6>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md">
                  <img src="/assets/images/html.png" />
                  <h4>HTML&CSS</h4>
                  <div className="text"><p>Donec sollicitudin molestie malesuada.
                  Curabitur aliquet quam id dui posuere
                    blandit. Nulla porttitor.</p>
                  </div>
                </div>
                <div className="col-md">
                  <img src="/assets/images/pan.png" />
                  <h4>GRAPHIC DESIGN</h4>
                  <div className="text"><p>Donec sollicitudin molestie malesuada.
                  Curabitur aliquet quam id dui posuere
                    blandit. Nulla porttitor.</p>
                  </div>
                </div>
                <div className="col-md">
                  <img src="/assets/images/clock.png" />
                  <h4>ON TIME PROJECTS</h4>
                  <div className="text"><p>Donec sollicitudin molestie malesuada.
                  Curabitur aliquet quam id dui posuere
                    blandit. Nulla porttitor.</p>
                  </div>
                </div>
                <div className="col-md">
                  <img src="/assets/images/fully.png" />
                  <h4>FULLY SUPPORT</h4>
                  <div className="text"><p>Donec sollicitudin molestie malesuada.
                  Curabitur aliquet quam id dui posuere
                    blandit. Nulla porttitor.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="corn">
            <img src="/assets/images/piccon.png" />
            <div className="corn-text">
              <p>Design must reflect the practical and aesthetic in business but above all...
                good design must primarily serve people.</p>
              <p>Thomas J. Watson </p>
            </div>
          </div>
          <div className="sevices">
            <div className="sevices-text">
              <h2>MODERN AND CLEAN DESIGN</h2>
              <h6 className="title-section">OUR PORTFOLIO</h6>
              <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tinci<br />
                dunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lac<br />
                inia in, elementum id enim. Vestibulum ac diam sit anet.</p>
            </div>
            <div className="sevices">
              <a href="#" className="col-md btn1">ALL</a>
              <a href="#" className="col-md btn1">GRAPHIC</a>
              <a href="#" className="col-md btn1">WEB</a>
              <a href="#" className="col-md btn1">PHOTOGAPHY</a>
            </div>
          </div>

          <div className="portfolio">
            <p>
              <a className="btn btn-primary" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                <img src="/assets/images/beer.jpg" />
              </a>
              <button className="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample-2" aria-expanded="false" aria-controls="collapseExample-2">
                <img src="/assets/images/forest.jpg" />
              </button>
              <button className="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <img src="/assets/images/letter.jpg" />
              </button>
              <button className="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <img src="/assets/images/city.jpg" />
              </button>
            </p>
            <div className="collapse" id="collapseExample">
              <div className="container">
                <div className="row portfolio-lio">
                  <div className="col-md-6 img">
                    <img src="/assets/images/beer.jpg" />
                  </div>
                  <div className="col-md-6 text">
                    <h4>LOREM IPSUM IMAGE</h4>
                    <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit.
                    Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elemena port
                    titor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Ves
                    tibulum ac diam sit anet.
                    Oitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla portti
                    tor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accum
                    san tincidunt. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ac
                        diam sit anet.</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="collapse" id="collapseExample-2">
              <div className="container">
                <div className="row portfolio-lio">
                  <div className="col-md-6 img">
                    <img src="/assets/images/beer.jpg" />
                  </div>
                  <div className="col-md-6 text">
                    <h4>kla</h4>
                    <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit.
                    Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elemena port
                    titor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Ves
                    tibulum ac diam sit anet.
                    Oitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla portti
                    tor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accum
                    san tincidunt. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ac
                        diam sit anet.</p>
                  </div>
                </div>
              </div>
            </div>
            <p>
              <button className="btn btn-primary" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                <img src="/assets/images/apple.jpg" />
              </button>
              <button className="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <img src="/assets/images/bookk.jpg" />
              </button>
              <button className="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <img src="/assets/images/shield.jpg" />
              </button>
              <button className="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <img src="/assets/images/pp.jpg" />
              </button>
            </p>
            <img src="/assets/images/dow.png" />

            <div className="portfolio-text">
              <h4>WE LOVE OUR WORK</h4>
              <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tinci<br />
                dunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidun.</p>
              <div className="portfolio-buttom">
                <a href="#" className="btn1">Purshase Theme</a>
              </div>
            </div>
          </div>
          <div className="aboutus">
            <div className="fully">
              <h2>FULLY RESPONSIVE DESIGN</h2>
              <h6 className="title-section1">ABOUT US</h6>
              <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tinci<br />
                    dunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lac<br />
                    inia in, elementum id enim. Vestibulum ac diam sit anet.</p>
              <div className="container">
                <div className="row fully">
                  <div className="col-md-3">
                    <img src="/assets/images/man.png" />
                    <h6>John Doe</h6>
                    <p>Creative Director</p>
                  </div>
                  <div className="col-md-3">
                    <img src="/assets/images/famal.png" />
                    <h6>Lorem Ipsum</h6>
                    <p>Graphic Designer</p>
                  </div>
                  <div className="col-md-3">
                    <img src="/assets/images/man.png" />
                    <h6>Lorem Doe</h6>
                    <p>Web Developer</p>
                  </div>
                  <div className="col-md-3">
                    <img src="/assets/images/femalela.png" />
                    <h6>John Ipsum</h6>
                    <p>Photographer</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="mystory">
        <div className="container">
            <h5>MyStory</h5>
            <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor 
                accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ac diam sit anet.
                Oitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tinci
                dunt. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ac diam sit anet.</p>
            <img src="/assets/images/time.gif" />
            <p>2 years in Artica</p>
            <img src="/assets/images/one.gif" />
            <p>From NY</p>
        </div>
    </div>

    <div className="container skills">
        <div className="skills-text">
            <h2>WE HAVE SKILLS</h2>
            <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tinci<br />
                dunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidun.</p>
        </div>
        
        <div className="row group">
            <div className="col-md-6 tab">
                <h3>Our Company</h3>
                <p>Cras ultricies ligula sed magna dictum porta. Curabitur arcu erat, accumsan id imperdiet 
                    et, porttitor at sem. Nulla quis lorem ut libero malesuada feugiat. Nulla porttitor accum
                    san tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat. Quisque velit nisi, 
                    pretium ut lacinia in, elementum id enim. Quisque velit nisi, pretium ut lacinia in, 
                    elementum id enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices 
                    posuere cubilia Curae; Donec velit neque, auctor situr arcu erat, accumsan id imperdiet 
                    et, porttitor at sem.</p>
                    <a href="#" className="btn1">Read More</a>
            </div>
            <div className="col-md-6">
                <h3>Our Skills</h3>
                    <div className="skills html">
                        HTML&CSS
                    </div>
                    <div className="skills css">
                        Javascript
                    </div>
                    <div className="skills js">
                        Graphic Design
                    </div>
                    <div className="skills php">
                        Photography
                    </div>
            </div>
        </div>
    </div>

    <div className="row iconbar">
        <div className="col-md-3"> 
            <img src="/assets/images/iconone.png" />
            <h3>230</h3>
            <p>Customers</p>
        </div>
        <div className="col-md-3">
            <img src="/assets/images/two.png" />
            <h3>22</h3>
            <p>Awards</p>
        </div>
        <div className="col-md-3">
            <img src="/assets/images/tree.png" />
            <h3>2380</h3>
            <p>Completed Projects</p>
        </div>
        <div className="col-md-3">
            <img src="/assets/images/four.png" />
            <h3>50890</h3>
            <p>Followers</p>
        </div>
    </div>

    <div className="container blog">
        <div className="blog-text">
            <h3>BOOTSTRAP GRID READY</h3>
            <h5 className="title-section">OUR BLOG</h5>
            <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tinci
                dunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lac
                inia in, elementum id enim. Vestibulum ac diam sit anet.</p>
        </div>


        <div className="row">
            <div className="col-md-6">
                <div className="row">
                    <div className="col-md-6"><img src="/assets/images/mit.jpg" /> 
                    </div>
                    <div className="col-md-6 bg">
                        <h4>Great party we have there</h4>
                        <h6>Published by: John Doe</h6>
                        <p>Donec sollicitudin molestie male<br/>suada. Curabitur aliquet quam id<br/>dui posuere blandit. Nulla porttitor<br/>accumsan tincidunt. Quisque velit <br/>nisi, pretium ut lacinia in, elemena <br/>porttitor accumsan tincidunt. <br/>Quisque velit nid enim. Vestibulum</p>
                    </div>
                </div>
            </div>
            <div className="col-md-6">
                <div className="row">
                    <div className="col-md-6"><img src="/assets/images/carmara.jpg" />
                    </div>
                    <div className="col-md-6 bg">
                        <h4>Great party we have there</h4>
                        <h6>Published by: John Doe</h6>
                        <p>Donec sollicitudin molestie male<br />suada. Curabitur aliquet quam id<br />dui posuere blandit. Nulla porttitor<br />accumsan tincidunt. Quisque velit <br />nisi, pretium ut lacinia in, elemena <br />porttitor accumsan tincidunt. <br />Quisque velit nid enim. Vestibulum</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div className="row">
            <div className="col-md-8">
                <div className="row">
                    <div className="col">
                        <div className="bg">
                        <h4>Great party we have there</h4>
                        <h6>Published by: John Doe</h6>
                        <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan 
                            tincidunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nid enim. 
                            Vestibulum ac diam sit anet.Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. 
                            Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nid enim. Vestibu
                            lum ac diam sit anet.
                            
                            Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan 
                            tincidunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nid enim. 
                            Vestibulum ac diam sit anet. </p>
                    </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="bg">
                        <h4>Great party we have there</h4>
                        <h6>Published by: John Doe</h6>
                        <p>Donec sollicitudin molestie male suada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nid enim. Vestibulum ac diam sit anet.</p>
                    </div>
                    </div>
                    <div className="col-md-6">
                        <div className="bg">
                        <h4>Great party we have there</h4>
                        <h6>Published by: John Doe</h6>
                        <p>Donec sollicitudin molestie male suada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nid enim. Vestibulum ac diam sit anet.</p>
                    </div>
                    </div>
                </div>
            </div>
            <div className="col-md-4 bg">
                <img src="/assets/images/building.jpg" />
                <h4>Great party we have there</h4>
                <h6>Published by: John Doe</h6>
                <p>Donec sollicitudin molestie male suada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Quisque velit  nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nid enim. Vestibulum ac diam sit anet.</p>
            </div>

    <div className="container subscribe">
        <div className="subscribe-text">
            <img src="/assets/images/dow.png" />
            <h2>SUBSCRIBE TO OUR NEWSLETTER</h2>
            <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tinci
                dunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidun.</p>
        </div>
        <div className="subscribe-button">
            <input placeholder="Your Mail" pattern="Your Mail" />
            <a href="#" className="col-md btn1">subscribe</a>
        </div>
    </div>
    </div>

    <div className="ourcustomers">
        <div className="container">
            <div className="material">
                <h3>OUR CUSTOMERS</h3>
                <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tinci<br />
                    dunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidun.</p>
                <img src="/assets/images/text1.png"/>
                <img src="/assets/images/text2.png"/>
                <img src="/assets/images/text3.png"/>
                <img src="/assets/images/text4.png"/>
                <img src="/assets/images/text5.png"/>
                <img src="/assets/images/text6.png"/>
            </div>
        </div>
    </div>

    <div className="container testimonials">
            <h3>TESTIMONIALS</h3>
            <h6 className="title-section">“ Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. <br />
                Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nisi. ”</h6>
            <h6>John Doe</h6>
    </div>

    <div className="contact">
        <div className="container">
            <h4>FEEL FREE TO ASK US ANYTHING</h4>
            <h6 className="title-section1">CONTACT US</h6>
            <p>Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tinci
                dunt. Quisque velit nisi, pretium ut lacinia in, elemena porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lac
                inia in, elementum id enim. Vestibulum ac diam sit anet.</p>
            <div className="row contact">
                <div className="col-md-4"> 
                    <img src="/assets/images/icon1.png" />
                    <h6>ADDRESSE</h6>
                    <h6>Awesome Street 234,NY</h6>
                </div>
                <div className="col-md-4">
                    <img src="/assets/images/icon2.png" />
                    <h6>MALL</h6>
                    <h6>articamail@mail.com</h6>
                </div>
                <div className="col-md-4">
                    <img src="/assets/images/icon3.png" />
                    <h6>PHONE</h6>
                    <h6>(000)123-456-789</h6>
                </div>
        </div>
        <div className="box">
            <form >
                <div className="text">
                    <div className="box-tree">
                        <input placeholder="Subject*" pattern="Subject" />
                        <input placeholder="Name*" pattern="Name" />
                        <input placeholder="Email*" pattern="Email" />
                </div>
                <div className="form-group">
                    <label for="exampleFormControlTextarea1"></label>
                    <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                  </div>
                    <div className="submit-button">
                        <a href="#"className="btn">submit</a>
                    </div>
            </div>
            </form>
        </div>
    </div>
    </div>

    <div className="last">
        <img src="/assets/images/logowhite.png" />
        <p>© Copyright 2013 Artica. All Rights Reserved.</p>
    </div>
        </div>
        </div>

      </>
    )
  }
}