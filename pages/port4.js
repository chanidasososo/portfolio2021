import React from "react";
import Head from "next/head";
import Link from "next/link";

export default class Port1 extends React.Component {
  render() {
    const ar = [];
    console.log(ar);
    return (
      <>
        <Head>
          <title>:: Port 4</title>
        </Head>
        <header>
          
        </header>
        
        <div className="port4">
          <div className="main">
            <div className="top">
              <div className="container">
                <div className="top-in">
                  <span>วันพุธ ที่ 7 ตุลาคม 2564</span>

                  <nav className="social-top">
                    <ul className="top-menu">
                      <Link href="">
                        <a>
                          <img src="assets/images/facebookw.png" alt="" />
                        </a>
                      </Link>
                      <Link href="">
                        <a>
                          <img src="assets/images/twitterw.png" alt="" />
                        </a>
                      </Link>
                      <Link href="">
                        <a>
                          <img src="assets/images/linew.png" alt="" />
                        </a>
                      </Link>
                      <Link href="">
                        <a>
                          <img src="assets/images/search.png" alt="" />
                        </a>
                      </Link>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>

            <div className="logo">
              <Link href="">
                <a>
                  <img src="assets/images/logo.svg" alt="" />
                </a>
              </Link>
            </div>

            {/* <div className="menu-mobile">
              <img src="assets/images/hamberger.svg" alt="" />
            </div> */}

            <div className="nav-manu">
              <div className="container">
                <ul>
                  <li>
                    <Link href="">
                      <a>
                        <span>หน้าแรก</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a>
                        <span>ข่าว</span>
                      </a>
                    </Link>

                    <div className="submenu">
                      <Link href="">
                        <a>Submenu 1</a>
                      </Link>
                      <Link href="">
                        <a>Submenu 1</a>
                      </Link>
                      <Link href="">
                        <a>Submenu 1</a>
                      </Link>
                      <Link href="">
                        <a>Submenu 1</a>
                      </Link>
                      <Link href="">
                        <a>Submenu 1</a>
                      </Link>
                      <Link href="">
                        <a>Submenu 1</a>
                      </Link>
                    </div>
                  </li>
                  <li>
                    <Link href="">
                      <a>
                        <span>วิเคราะห์การเมือง</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a>
                        <span>เศรษฐกิจ-ธุรกิจ</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a>
                        <span>ยานยนต์</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a>
                        <span>เทคโนโลยี</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a>
                        <span>คุณภาพชีวิต-สังคม</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a>
                        <span>สุขภาพ</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a>
                        <span>ต่างประเทศ</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a>
                        <span>ไลฟ์สไตล์</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a>
                        <span>คอลัมนิสต์</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a>
                        <span>Biz2U</span>
                      </a>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>

            <div className="breadcrumb">
              <div className="container">
                <ul>
                  <li>
                    <Link href="">
                      <a>หน้าแรก</a>
                    </Link>
                    <div className="submanu-breab">
                      <Link href="">
                        <a>menu1</a>
                      </Link>
                      <Link href="">
                        <a>menu2</a>
                      </Link>
                      <Link href="">
                        <a>menu3</a>
                      </Link>
                      <Link href="">
                        <a>menu</a>
                      </Link>
                    </div>
                  </li>

                  <li>
                    <Link href="">
                      <a>คุณภาพชีวิต</a>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>

            <div className="detail-content">
              <div className="container">
                <div className="row">
                  <div className="col-md-8 ">
                    <Link href="">
                      <a className="btn-1 btn-hilight">คุณภาพชีวิต-สังคม</a>
                    </Link>
                    <h1 className="content-title">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    </h1>
                    <div className="content-mete">
                      <div className="date">
                        <span>07 ต.ค. 2564 เวลา 8:20 น.</span>
                      </div>
                      <div className="views">909</div>
                    </div>

                    <div className="picture">
                      <img src="assets/images/woman.jpg" alt="" />
                    </div>

                    <div className="section">
                      <span>
                        Lorem ipsum dolor sit amet consectetur. Lorem ipsum
                        dolor sit amet consectetur.{" "}
                      </span>
                    </div>
                    <div className="detail">
                      <p>
                        Lorem ipsum, dolor sit amet consectetur adipisicing
                        elit. Culpa, delectus deleniti blanditiis voluptatum
                        consequuntur, sit aperiam reprehenderit eius molestiae
                        quis quo vero, voluptatibus earum distinctio odit
                        expedita aliquid porro ut. Lorem ipsum dolor sit amet,
                        consectetur adipisicing elit. Aperiam magni praesentium,
                        nulla ipsum doloremque expedita officia quasi, ad in
                        iure consequatur tempora itaque iusto eum facilis
                        excepturi, eligendi assumenda. Provident.
                      </p>
                      <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Voluptate error itaque consequuntur, eaque quas
                        explicabo culpa tempora voluptatibus a, nihil ab
                        adipisci architecto. Voluptatem nemo explicabo eaque
                        totam sint autem?
                      </p>
                    </div>

                    <div className="picture">
                      <img src="assets/images/woman.jpg" alt="" />
                    </div>
                    <div className="detail">
                      <p>
                        Lorem ipsum, dolor sit amet consectetur adipisicing
                        elit. Culpa, delectus deleniti blanditiis voluptatum
                        consequuntur, sit aperiam reprehenderit eius molestiae
                        quis quo vero, voluptatibus earum distinctio odit
                        expedita aliquid porro ut. Lorem ipsum dolor sit amet,
                        consectetur adipisicing elit. Aperiam magni praesentium,
                        nulla ipsum doloremque expedita officia quasi, ad in
                        iure consequatur tempora itaque iusto eum facilis
                        excepturi, eligendi assumenda. Provident.
                      </p>
                      <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Voluptate error itaque consequuntur, eaque quas
                        explicabo culpa tempora voluptatibus a, nihil ab
                        adipisci architecto. Voluptatem nemo explicabo eaque
                        totam sint autem?
                      </p>
                    </div>
                    <div className="picture">
                      <img src="assets/images/woman.jpg" alt="" />
                    </div>
                    <div className="tags">
                      <ul>
                        <li>แท็กที่เกี่ยวข้อง</li>
                        <li>
                          <Link href="">
                            <a>เฝ้าระวัง</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>แม่น้ำเจ้าพระยา</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>ระดับน้ำ</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>สถานการณ์น้ำท่วม</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>น้ำท่วมกรุงเทพ</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>พายุเข้าไทย</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>นำ้ท่วม กทม. จุดอ่อนน้ำท่วม</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>น้ำท่วม กทม.ที่ไหน</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>น้ำท่วมล่าสุด</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>สถานการณ์พายุล่าสุด</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>จุดเสี่ยงน้ำท่วม</a>
                          </Link>
                        </li>
                      </ul>
                    </div>

                    <div className="social">
                      <Link href="">
                        <a>
                          <img src="assets/images/facebook.png" alt="" />
                        </a>
                      </Link>
                      <Link href="">
                        <a>
                          <img src="assets/images/twitter.png" alt="" />
                        </a>
                      </Link>
                      <Link href="">
                        <a>
                          <img src="assets/images/line.png" alt="" />
                        </a>
                      </Link>
                      <Link href="">
                        <a>
                          <img
                            src="assets/images/black-back-closed-envelope-shape.png"
                            alt=""
                          />
                        </a>
                      </Link>
                    </div>

                    <div className="box">
                      <span>เนื้อหาที่ได้รับการโปรโมต</span>
                    </div>
                  </div>

                  <div className="col-md-4 col-right">
                    <div className="share-news">
                      <span>แชร์ข่าว</span>
                      <div className="share">
                        <Link href="">
                          <a>
                            <img src="assets/images/facebook.png" alt="" />
                          </a>
                        </Link>
                        <Link href="">
                          <a>
                            <img src="assets/images/twitter.png" alt="" />
                          </a>
                        </Link>
                        <Link href="">
                          <a>
                            <img src="assets/images/line.png" alt="" />
                          </a>
                        </Link>
                        <Link href="">
                          <a>
                            <img
                              src="assets/images/black-back-closed-envelope-shape.png"
                              alt=""
                            />
                          </a>
                        </Link>
                      </div>
                    </div>
                    <div className="news-list">
                      <h3 className="list-head">
                        <span>ข่าวล่าสุด</span>
                        <a href="" className="viewmore">
                          ดูทั้งหมด
                        </a>
                      </h3>
                      <ul>
                        <li>
                          <Link href="">
                            <a>
                              <div className="news-left">
                                <div className="thumb1">
                                  <img src="assets/images/i.webp" alt="" />
                                </div>
                              </div>

                              <div className="news-right">
                                <div className="news-detail">
                                  <h4 className="news-title">
                                    ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                    ลุยตลาดกัญชงครบวงจร ปี 2565
                                  </h4>
                                  <div className="news-date">
                                    <img
                                      src="assets/images/time-icon.svg"
                                      alt=""
                                    />
                                    <span>09 ต.ค. 2564 | 18:45</span>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>

                        <li>
                          <Link href="">
                            <a>
                              <div className="row">
                                <div className="col-4">
                                  <div className="thumb1">
                                    <img src="assets/images/i.webp" alt="" />
                                  </div>
                                </div>

                                <div className="col-8 ml-0">
                                  <div className="news-detail">
                                    <h4 className="news-title">
                                      ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                      ลุยตลาดกัญชงครบวงจร ปี 2565
                                    </h4>
                                    <div className="news-date">
                                      09 ต.ค. 2564 | 18:45
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>
                              <div className="row">
                                <div className="col-4">
                                  <div className="thumb1">
                                    <img src="assets/images/i.webp" alt="" />
                                  </div>
                                </div>

                                <div className="col-8">
                                  <div className="news-detail">
                                    <h4 className="news-title">
                                      ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                      ลุยตลาดกัญชงครบวงจร ปี 2565
                                    </h4>
                                    <div className="news-date">
                                      09 ต.ค. 2564 | 18:45
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>
                              <div className="row">
                                <div className="col-4">
                                  <div className="thumb1">
                                    <img src="assets/images/i.webp" alt="" />
                                  </div>
                                </div>

                                <div className="col-8">
                                  <div className="news-detail">
                                    <h4 className="news-title">
                                      ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                      ลุยตลาดกัญชงครบวงจร ปี 2565
                                    </h4>
                                    <div className="news-date">
                                      09 ต.ค. 2564 | 18:45
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div className="news-list">
                      <h3 className="list-head">
                        <span>ข่าวที่เกี่ยวข้อง</span>
                        {/* <a href="" className="viewmore">
                      ดูทั้งหมด
                    </a> */}
                      </h3>
                      <ul>
                        <li>
                          <Link href="">
                            <a>
                              <div className="news-left">
                                <div className="thumb1">
                                  <img src="assets/images/i.webp" alt="" />
                                </div>
                              </div>

                              <div className="news-right">
                                <div className="news-detail">
                                  <h4 className="news-title">
                                    ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                    ลุยตลาดกัญชงครบวงจร ปี 2565
                                  </h4>
                                  <div className="news-date">
                                    <img
                                      src="assets/images/time-icon.svg"
                                      alt=""
                                    />
                                    <span>09 ต.ค. 2564 | 18:45</span>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>

                        <li>
                          <Link href="">
                            <a>
                              <div className="row">
                                <div className="col-4">
                                  <div className="thumb1">
                                    <img src="assets/images/i.webp" alt="" />
                                  </div>
                                </div>

                                <div className="col-8 ml-0">
                                  <div className="news-detail">
                                    <h4 className="news-title">
                                      ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                      ลุยตลาดกัญชงครบวงจร ปี 2565
                                    </h4>
                                    <div className="news-date">
                                      09 ต.ค. 2564 | 18:45
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>
                              <div className="row">
                                <div className="col-4">
                                  <div className="thumb1">
                                    <img src="assets/images/i.webp" alt="" />
                                  </div>
                                </div>

                                <div className="col-8">
                                  <div className="news-detail">
                                    <h4 className="news-title">
                                      ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                      ลุยตลาดกัญชงครบวงจร ปี 2565
                                    </h4>
                                    <div className="news-date">
                                      09 ต.ค. 2564 | 18:45
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>
                              <div className="row">
                                <div className="col-4">
                                  <div className="thumb1">
                                    <img src="assets/images/i.webp" alt="" />
                                  </div>
                                </div>

                                <div className="col-8">
                                  <div className="news-detail">
                                    <h4 className="news-title">
                                      ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                      ลุยตลาดกัญชงครบวงจร ปี 2565
                                    </h4>
                                    <div className="news-date">
                                      09 ต.ค. 2564 | 18:45
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>
                      </ul>
                    </div>

                    <div className="news-list">
                      <h3 className="list-head">
                        <span>ข่าวที่น่าสนใจ</span>
                        {/* <a href="" className="viewmore">
                      ดูทั้งหมด
                    </a> */}
                      </h3>
                      <ul>
                        <li>
                          <Link href="">
                            <a>
                              <div className="news-left">
                                <div className="thumb1">
                                  <img src="assets/images/i.webp" alt="" />
                                </div>
                              </div>

                              <div className="news-right">
                                <div className="news-detail">
                                  <h4 className="news-title">
                                    ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                    ลุยตลาดกัญชงครบวงจร ปี 2565
                                  </h4>
                                  <div className="news-date">
                                    <img
                                      src="assets/images/time-icon.svg"
                                      alt=""
                                    />
                                    <span>09 ต.ค. 2564 | 18:45</span>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>

                        <li>
                          <Link href="">
                            <a>
                              <div className="row">
                                <div className="col-4">
                                  <div className="thumb1">
                                    <img src="assets/images/i.webp" alt="" />
                                  </div>
                                </div>

                                <div className="col-8 ml-0">
                                  <div className="news-detail">
                                    <h4 className="news-title">
                                      ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                      ลุยตลาดกัญชงครบวงจร ปี 2565
                                    </h4>
                                    <div className="news-date">
                                      09 ต.ค. 2564 | 18:45
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>
                              <div className="row">
                                <div className="col-4">
                                  <div className="thumb1">
                                    <img src="assets/images/i.webp" alt="" />
                                  </div>
                                </div>

                                <div className="col-8">
                                  <div className="news-detail">
                                    <h4 className="news-title">
                                      ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                      ลุยตลาดกัญชงครบวงจร ปี 2565
                                    </h4>
                                    <div className="news-date">
                                      09 ต.ค. 2564 | 18:45
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>
                        <li>
                          <Link href="">
                            <a>
                              <div className="row">
                                <div className="col-4">
                                  <div className="thumb1">
                                    <img src="assets/images/i.webp" alt="" />
                                  </div>
                                </div>

                                <div className="col-8">
                                  <div className="news-detail">
                                    <h4 className="news-title">
                                      ‘สมุนไพรวังพรม’ ต่อยอดสินค้า แตกไลน์ธุรกิจ
                                      ลุยตลาดกัญชงครบวงจร ปี 2565
                                    </h4>
                                    <div className="news-date">
                                      09 ต.ค. 2564 | 18:45
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                  {/* end .col-right  */}
                </div>
              </div>
            </div>

            <div className="end">
              <div className="container">
                <div className="row">
                  <div className="col-lg-4 col-sm-12 col-xs-12 mb-5 mb-md-0">
                    <h3 className="point">โฆษณาบนเว็บไซต์</h3>
                    <div className="topic">
                      <p>อัลเลียซ สะอิ</p>
                      <p>
                        Direct ฝ่ายโฆษณา กรุงเทพธุรกิจ : 02-338-3561 Moblie :
                        087-519-1379
                      </p>
                      <p>allias_sae@nationgroup.com</p>
                      <p>ศิชล ภวัตโณทัย</p>
                      <p>
                        Direct ฝ่ายโฆษณา กรุงเทพธุรกิจ : 02-338-3325 Moblie :
                        085-255-6753
                      </p>
                      <p>sichol_paw@nationgroup.com</p>
                      <p>สมัครสมาชิก นสพ.กรุงเทพธุรกิจ 0-2338-3000 กด 1</p>
                    </div>
                  </div>
                  <div className="col-lg-4 col-sm-12 col-xs-12 mb-5 mb-md-0">
                    <div className="point">หมวดหมู่</div>
                    <div className="row">
                      <div className="col-6 col-md-auto fond">
                        <a href="">ข่าว</a>
                      </div>
                      <div className="col-6 col-md-auto fond">
                        <a href="">วิเคราะห์การเมือง</a>
                      </div>
                      <div className="col-6 col-md-auto fond">
                        <a href="">เศรษฐกิจ-ธุระกิจ</a>
                      </div>
                      <div className="col-6 col-md-auto fond">
                        <a href="">ยานยนต์</a>
                      </div>
                      <div className="col-6 col-md-auto fond">
                        <a href="">เทคโนโลยี</a>
                      </div>
                      <div className="col-6 col-md-auto fond">
                        <a href="">คุณภาพชีวิต-สังคม</a>
                      </div>
                      <div className="col-6 col-md-auto fond">
                        <a href="">สุขภาพ</a>
                      </div>
                      <div className="col-6 col-md-auto fond">
                        <a href="">ต่างประเทศ</a>
                      </div>
                      <div className="col-6 col-md-auto fond">
                        <a href="">ไลฟ์สไตล์</a>
                      </div>
                      <div className="col-6 col-md-auto fond">
                        <a href="">คอลัมนิสต์</a>
                      </div>
                      <div className="col-6 col-md-auto fond">
                        <a href="">Biz2u</a>
                      </div>
                      <div className="col-6 col-md-auto fond">
                        <a href="">นโยบายส่วนตัว</a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-sm-12 col-xs-12 mb-5 mb-md-0">
                    <div className="point">พาร์ทเนอร์</div>
                    <div className="row">
                      <div className="col-4 fond">
                        <a href="">เนชั่นไทยแลนด์</a>
                      </div>
                      <div className="col-4 fond">
                        <a href="">Nation Group</a>
                      </div>
                      <div className="col-4 fond">
                        <a href="">คม ชัด ลึก</a>
                      </div>
                      <div className="col-4 fond">
                        <a href="">กรุงเทพธุรกิจ</a>
                      </div>
                      <div className="col-4 fond">
                        <a href="">Nation</a>
                      </div>
                      <div className="col-4 fond">
                        <a href="">Spring News</a>
                      </div>
                      <div className="col-4 fond">
                        <a href="">Thainewsonline</a>
                      </div>
                      <div className="col-4 fond">
                        <a href="">ฐานเศรษฐกิจ</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="copyright">
              <div className="text">
                <span>
                  © 2021 กรุงเทพธุรกิจ มีเดีย จำกัด. All Rights Reserved.
                </span>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
