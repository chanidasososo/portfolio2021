$(function() {
  $('.btn-scrolltop-rotate').on('click', function() {
      $("html, body").animate({
          scrollTop: 0
      }, 500);
  })

  $(window).on('scroll', function() {
      var scroll_top = $(window).scrollTop()
          // console.log(scroll_top);
      if (scroll_top > 300) {
          $('.btn-scrolltop-rotate').addClass('show-btn');
      } else {
          $('.btn-scrolltop-rotate').removeClass('show-btn');
      }
  });
});